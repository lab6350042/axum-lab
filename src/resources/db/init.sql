CREATE TABLE post (
    id SERIAL PRIMARY KEY,
    title VARCHAR(255),
    image_url VARCHAR(255),
    date_picture TIMESTAMP,
    description TEXT,
    family VARCHAR(100),
    gender VARCHAR(50),
    specie VARCHAR(100),
    location INT,
    locality VARCHAR(100),
    verified BOOLEAN,
    published_at TIMESTAMP,
    updated_at TIMESTAMP
);
