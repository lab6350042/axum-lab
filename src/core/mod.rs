use dotenv::dotenv;
use std::time::Duration;

use sqlx::postgres::PgPoolOptions;
use tracing::level_filters::LevelFilter;

pub struct Config {
    pub host: String,
    pub port: u16,
    pub logging: String,
    pub database_url: String,
}

impl Config {
    pub fn load() -> Self {
        dotenv().ok();

        let host = std::env::var("DOMAIN").unwrap_or("0.0.0.0".to_string());
        let port = std::env::var("PORT")
            .unwrap_or("3000".to_string())
            .parse::<u16>()
            .unwrap();
        let level = std::env::var("LOGGING_LEVEL").unwrap_or("INFO".to_string());
        let database_url = std::env::var("DATABASE_URL")
            .unwrap_or("postgres://postgres:password@localhost".to_string());

        Self {
            host,
            port,
            logging: level,
            database_url,
        }
    }

    pub fn tracing(level: String) {
        tracing_subscriber::fmt()
            .with_max_level(level.parse::<LevelFilter>().unwrap())
            .init();
    }

    pub async fn db_pool(database_url: String) {
        PgPoolOptions::new()
            .max_connections(5)
            .acquire_timeout(Duration::from_secs(3))
            .connect(database_url.as_str())
            .await
            .expect("can't connect to database");
    }
}
