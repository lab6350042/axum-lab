use axum::Router;
use tower_http::trace::TraceLayer;

mod controller;
mod core;

use core::Config;

#[tokio::main]
async fn main() {
    let config = Config::load();
    let (host, port) = (config.host, config.port);

    Config::tracing(config.logging);
    Config::db_pool(config.database_url).await;

    let app = Router::new()
        .nest("/axum-lab", controller::routes())
        .fallback(controller::fallback)
        .layer(TraceLayer::new_for_http());

    let listener = tokio::net::TcpListener::bind((host, port)).await.unwrap();
    tracing::debug!("listening on {:?}", listener.local_addr().unwrap());

    axum::serve(listener, app).await.unwrap();
}
