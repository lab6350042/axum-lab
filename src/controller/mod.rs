use axum::{http::StatusCode, response::IntoResponse, routing::get, Json, Router};
use serde::Serialize;

#[derive(Serialize)]
pub struct Response {
    message: String,
}

pub async fn healthcheck() -> impl IntoResponse {
    let response = Response {
        message: "Everything is working fine".to_string(),
    };

    (StatusCode::OK, Json(response))
}

pub async fn fallback() -> (StatusCode, Json<Response>) {
    let response = Response {
        message: "Resource not found".to_string(),
    };

    (StatusCode::NOT_FOUND, Json(response))
}

pub fn routes() -> Router {
    Router::new().route("/health", get(healthcheck))
}
