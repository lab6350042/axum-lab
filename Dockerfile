FROM rust:1.76.0-slim-bullseye as builder

WORKDIR /app

COPY Cargo.toml Cargo.lock ./

COPY src ./src/

RUN cargo build --release

FROM debian:bullseye-slim

WORKDIR /usr/local/bin

COPY --from=builder /app/target/release/axum-lab app

EXPOSE 3000

CMD ["./app"]
